import { Component, EventEmitter, OnInit, Output, ViewChild, OnDestroy } from '@angular/core';
import {Router} from "@angular/router"
import { NgForm } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  @ViewChild('f') loginForm: NgForm;
  @ViewChild('f') forgettenPasswordForm: NgForm;
  @ViewChild('f') aprroveCodeForm: NgForm;
  @ViewChild('f') registerForm: NgForm;

  isLoginMode: boolean = true;
  private authenticationSub : Subscription;
  isAutheticated: boolean;
  askingServer: boolean= false;
  errorMessage: string = null;
  isPasswordForgetten: boolean = false;
  approvePassword: boolean = false;
  isRegistrationSuccessful = false;
  userInfo: {email: string, password: string, remember_me: boolean} = {email: null, password: null, remember_me:null};
  code: string;

  @Output() loginInEvent = new EventEmitter<boolean>();

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.authenticationSub = this.authService.isAutheticated.subscribe(isLogin => this.isAutheticated = isLogin);
    if(this.isAutheticated) {
      this.router.navigate(['/home']);
    }
  }

  loginSubmit (): void {
    if(!this.loginForm.valid) return;
    this.askingServer = true;
    const remember_me = this.loginForm.value.remember_me === true ? true : false;
    this.userInfo = {email: this.loginForm.value.email, password: this.loginForm.value.password, remember_me: remember_me};
    this.authService.login(this.userInfo).subscribe(
        isOk => {
          if(isOk){
            this.loginForm.reset();
            this.authService.changeUserAuthentication(true);
            this.router.navigate(['/home']);
          } else{
            this.errorMessage = "Service is unavailable. Please try again later."
          }
        },
        err => {
          console.log(err);
            this.errorMessage = err;
            this.loginForm.reset();
        }
    );
    this.askingServer = false;
  }

  onRegistrationSubmit(): void {
    this.askingServer = true;
    console.log(this.registerForm.value)
    this.authService.register(this.registerForm.value).subscribe(
      isOk => {
        if (isOk){
          this.isRegistrationSuccessful = true;
          this.authService.changeUserAuthentication(true);
          // Basarili kayittan sonra islemin basarili oldugu ayni ekranda musteriye bildirilecek,
          // OK benzeri bir butonla route calisacak.
          this.router.navigate(['/home']);
        } else {
          this.registerForm.reset();
          this.errorMessage = 'Email adress is alread in use!';
        }
      },
      errRes => {
        this.errorMessage = errRes;
      }
    );
    this.askingServer = false;
  }

  forgettenPasswordSubmit(){
    console.log('forgettenPasswordSubmit' + this.forgettenPasswordForm.value);
    this.userInfo.email = this.forgettenPasswordForm.value.email;
    this.authService.getCode(this.userInfo.email).subscribe(code=> this.code = code);
    this.toggleForgettenPassword();
    this.approvePassword = true;
  }

  aprroveCodeSubmit(){
    console.log('aprroveCodeSubmit' + this.aprroveCodeForm.value)
    if(this.aprroveCodeForm.value.code == this.code){
      console.log("Open new password page and change it.")
    }
  }

  toggleForgettenPassword() {
    this.isPasswordForgetten = !this.isPasswordForgetten;
  }

  toggleLoginMode(){
    this.isLoginMode = !this.isLoginMode;
  }

  ngOnDestroy(): void {
    this.authenticationSub.unsubscribe();
  }
}
