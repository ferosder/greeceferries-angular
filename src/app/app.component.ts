import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from './_modals/user';
import { AuthService } from './_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title: 'greeceferies';

  userSub: Subscription;
  user: User;
  private authenticationSub : Subscription;
  isAutheticated = true;
  username: string;
  role: string;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authenticationSub = this.authService.isAutheticated.subscribe(isLogin => this.isAutheticated = isLogin);
    this.userSub = this.authService.user.subscribe(user=> this.user = user);
    this.authService.setUser();
  }

  logout(): void {
    this.authService.changeUserAuthentication(false);
    this.authService.signOut();
  }

  ngOnDestroy(): void {
    this.authenticationSub.unsubscribe();
    this.userSub.unsubscribe();
  }
}
